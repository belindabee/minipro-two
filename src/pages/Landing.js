import React, { Component } from "react";
import Carousel from "../components/Carousel";
import MovieList from "../components/MovieList";
import { getAllMovies } from "../util/api";

export default class Landing extends Component {
  constructor(props) {
    super(props);

    this.state = {
      movie: []
    };

    getAllMovies().then(res => {
      const movie = res.data.data.docs;
      this.setState({
        movie
      });
    });
  }
  render() {
    return (
      <div>
        <Carousel />
        <div className="content">
          <h2 style={{ color: "#8C8989" }}>Browse by Category</h2>
          <div className="content-links">
            {this.state.movie.map(item => (
              <div class="link-button">item.category</div>
            ))}
          </div>
        </div>

        <MovieList />
      </div>
    );
  }
}
