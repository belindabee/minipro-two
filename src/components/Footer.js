import React, { Component } from "react";
import "../assets/css/style.css";

export default class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <div className="footer-section-brand">
          <div className="footer-brand">
            <div className="footer-logo">
              <i className="fa fa-play"></i>
            </div>
            <p>BelindaTV</p>
          </div>
          <br />
          <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard.printing and
            typesetting industry. Lorem Ipsum has been the industry's standard
          </p>
        </div>
        <div className="footer-section">
          <div className="footer-links">Tentang Kami</div>
        </div>
        <div className="footer-section">
          <div className="footer-links"></div>
        </div>
      </div>
    );
  }
}
