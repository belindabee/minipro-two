import React, { Component } from "react";

export default class MovieCard extends Component {
  render() {
    return (
      <div className="movie-card">
        <img src={this.props.image} alt={this.props.title} />
        <h2>{this.props.title}</h2>
        <p>{this.props.category}</p>
      </div>
    );
  }
}
