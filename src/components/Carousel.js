import React, { Component } from "react";
import img1 from "../assets/img/image1.jpg";
import img2 from "../assets/img/image2.jpg";
import Slider from "react-animated-slider";
import "react-animated-slider/build/horizontal.css";

export default class Carousel extends Component {
  render() {
    const sliderData = [
      {
        id: 1,
        img: img1,
        title: "Title"
      },
      {
        id: 2,
        img: img2,
        title: "Title_1"
      }
    ];
    return (
      <Slider>
        {sliderData.map((item, index) => (
          <div
            className="slider-content"
            key={index}
            style={{ background: `url('${item.img}') no-repeat center center` }}
          ></div>
        ))}
      </Slider>
    );
  }
}
