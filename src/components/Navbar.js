import React, { Component } from "react";
import "../assets/css/style.css";
import SigninModal from "./SigninModal";

export default class Navbar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showSignin: false,
      showProfileIcon: false,
      showProfile: false
    };
  }

  showSigninModal() {
    this.setState({
      showSignin: true
    });
  }
  render() {
    return (
      <div className="nav">
        <div className="nav-logo">
          <i className="fa fa-play"></i>
        </div>
        <span className="nav-header">BelindaTV</span>
        <input className="nav-search" type="text" placeholder="search movie" />
        <button className="nav-signin">Sign In</button>

        {/* <SigninModal /> */}
      </div>
    );
  }
}
