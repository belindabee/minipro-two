import React, { Component } from "react";
import "../assets/css/style.css";
import MovieCard from "./MovieCard";
import { getAllMovies } from "../util/api";

export default class MovieList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      movies: []
    };

    getAllMovies().then(res => {
      const movies = res.data.data.docs;
      this.setState({
        movies
      });
    });
  }
  render() {
    return (
      <div className="movie-list" width="20%">
        {this.state.movies.map((item, index) => (
          <MovieCard
            key={item._id}
            image={item.image}
            title={item.title}
            category={item.category}
          />
        ))}
        <MovieCard />
      </div>
    );
  }
}
